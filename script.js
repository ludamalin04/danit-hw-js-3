'use strict'
let num = +prompt('Enter your number');
if (num < 5) {
    alert('Sorry, no numbers');
} else {
    for (let startNum = 0; startNum <= num; startNum++) {
        if (startNum % 5 === 0) {
            console.log(startNum);
        }
    }
}